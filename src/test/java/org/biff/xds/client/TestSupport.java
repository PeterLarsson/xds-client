package org.biff.xds.client;

import lombok.SneakyThrows;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.InputStream;

/**
 * Created by Peter on 2016-05-17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public abstract class TestSupport {

    //
    @SneakyThrows
    InputStream load(final String name) {
        return new ClassPathResource(name).getInputStream();
    }
}
