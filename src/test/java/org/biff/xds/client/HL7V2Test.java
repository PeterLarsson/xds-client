package org.biff.xds.client;

import ca.uhn.hl7v2.model.v26.message.MDM_T02;
import ca.uhn.hl7v2.parser.PipeParser;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.openehealth.ipf.modules.cda.CDAR2Renderer;

import java.io.FileInputStream;

/**
 * Created by Peter on 2016-05-17.
 */
@Slf4j
public class HL7V2Test extends TestSupport {

    @Test
    public void parseTemplateTest() {
        final MDM_T02 message = parse("biff_4d_protocol.hl7", "UTF-8");

        log.debug("MSH-18: {}", message.getMSH().getCharacterSet(0).getValue());
        log.debug("MSH-21: {}", message.getMSH().getMsh21_MessageProfileIdentifier(1).getEntityIdentifier().getValue());
        log.debug("message: {}", message.getClass());
        log.debug("TXA-12: {}", message.getTXA().getTxa12_UniqueDocumentNumber().getNamespaceID().getValue());
    }

    @Test
    @SneakyThrows
    public void templateCDATest() {
        EKOProtocol ekoProtocol = EKOProtocol.valueOf(parse("biff_4d_protocol.hl7", "UTF-8"));

        CDAR2Renderer renderer = new CDAR2Renderer();
        renderer.render(ekoProtocol.getClinicalDocument(), System.err);

    }

    @Test
    @SneakyThrows
    public void siemensCDATest() {
        EKOProtocol ekoProtocol = EKOProtocol.valueOf(parse("siemens-poc-last.hl7", "ISO-8859-15"));

        CDAR2Renderer renderer = new CDAR2Renderer();
        renderer.render(ekoProtocol.getClinicalDocument(), System.err);

    }

    @Test
    @SneakyThrows
    public void siemensTest() {
        final PipeParser pipeParser = new PipeParser();
        final String source = IOUtils.toString(new FileInputStream("src/test/resources/16168122623301.hl7"), "ISO-8859-15");
        final Object o = pipeParser.parse(source);
        log.debug("parsed: {}", o.getClass());
    }

    @SneakyThrows
    private MDM_T02 parse(final String name, final String encoding) {
        PipeParser pipeParser = new PipeParser();
        final String source = IOUtils.toString(new FileInputStream("src/test/resources/" + name), encoding);
        final MDM_T02 message = (MDM_T02) pipeParser.parse(source);
        return message;
    }



}
