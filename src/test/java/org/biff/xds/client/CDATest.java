package org.biff.xds.client;

import ihe.iti.xds_b._2007.ProvideAndRegisterDocumentSetRequestType;
import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.openehealth.ipf.modules.cda.CDAR2Parser;
import org.openehealth.ipf.modules.cda.CDAR2Validator;
import org.openhealthtools.mdht.uml.cda.ClinicalDocument;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;

/**
 * Created by Peter on 2016-05-03.
 */
@Slf4j
public class CDATest extends TestSupport {

    @Autowired
    CDAR2Validator cdar2Validator;

    @Autowired
    CDAR2Parser cdar2Parser;


    @Autowired
    XDSClient xdsClient;


    @Test
    public void requestTest() {
        final ClinicalDocument clinicalDocument = doc("biff-example-cda.xml");

        final String documentUUID = xdsClient.uuid();
        final ProvideAndRegisterDocumentSetRequestType provideAndRegister = new ProvideAndRegisterDocumentSetRequestType();
        provideAndRegister.setSubmitObjectsRequest(xdsClient.createSubmitObjectsRequest(clinicalDocument, documentUUID, "test-cda.xml"));

        final ProvideAndRegisterDocumentSetRequestType.Document document = new ProvideAndRegisterDocumentSetRequestType.Document();
        document.setId(documentUUID);
        document.setValue(xdsClient.toBytes("src/test/resources/biff-example-cda.xml"));
        provideAndRegister.getDocument().add(document);

        xdsClient.dump(provideAndRegister);
    }

    @Test
    public void pafTest() {
        doc("14-4377_20160616_135009.xml");
        //doc("paf-2.xml");
    }


    @SneakyThrows
    ClinicalDocument doc(final String name) {
        @Cleanup
        final InputStream in = load("/" + name);
        ClinicalDocument clinicalDocument = cdar2Parser.parse(in);
        cdar2Validator.validate(clinicalDocument, null);
        return clinicalDocument;
    }

}
