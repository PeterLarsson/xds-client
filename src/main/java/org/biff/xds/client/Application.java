package org.biff.xds.client;

import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import sun.net.ftp.FtpClient;
import sun.net.ftp.FtpDirEntry;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.IllegalFormatCodePointException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


@SpringBootApplication(exclude = {
		EmbeddedServletContainerAutoConfiguration.class,
		WebMvcAutoConfiguration.class})
@Slf4j
public class Application implements CommandLineRunner {

	@Autowired
	XDSClient xdsClient;

	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if (args.length != 2) {
			log.error("usage: java -jar <app.jar> <cda-file-name | ftp-server> <xds-reposiory-url>");
			return;
		}

		String source = args[0];
		final String repositoryUrl = args[1];

		/*
		final AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(WebServiceConfig.class);
		final XDSClient client = ctx.getBean(XDSClient.class);
		*/

        if (source.contains("@") && source.contains(":")) {
            final int isuffix = source.lastIndexOf("@");
            final String ftpServer = source.substring(isuffix+1);
            final int iprefix = source.indexOf(":");
            final String ftpUser = source.substring(0, iprefix);
            String password = source.substring(iprefix+1, isuffix);
            processFtp(ftpServer, ftpUser, password.toCharArray(), repositoryUrl);
        } else  if (source.endsWith(".xml")) {
			xdsClient.storeCDAFile(source, repositoryUrl);
		} else if (source.endsWith(".hl7")) {
			xdsClient.storeHL7File(source, repositoryUrl);
		} else {
            throw new IllegalArgumentException("No such input source");
        }
	}

    //
    @SneakyThrows
    void processFtp(final String ftpServer, final String ftpUser, final char[] password, final String repositoryUrl) {
        if (log.isDebugEnabled()) {
            log.debug("login {}:{}@{}", ftpUser, new String(password), ftpServer);
        }
        final FtpClient ftpClient = FtpClient.create();
        ftpClient.connect(new InetSocketAddress(ftpServer, 21));
        ftpClient.login(ftpUser, password);
        ftpClient.enablePassiveMode(true);

        Stream.of(ftpClient.listFiles("*"))
                .map(ftpDirEntryIterator -> (Iterable<FtpDirEntry>) () -> ftpDirEntryIterator)
                .flatMap(iterable -> StreamSupport.stream(iterable.spliterator(), false))
                .map(ftpDirEntry -> ftpDirEntry.getName().toLowerCase())
                .filter(name -> isTarget(name))
                .forEach(name -> store(name, ftpClient, repositoryUrl));

        ftpClient.close();
    }

    //
    boolean isTarget(String name) {
        return name.endsWith(".xml") || name.endsWith(".hl7");
    }

    @SneakyThrows
    void store(String name, FtpClient ftpClient, String repositoryUrl) {
        log.debug("store file: {}", name);
        copy(name, ftpClient);
        if (name.endsWith(".xml")) {
            xdsClient.storeCDAFile(name, repositoryUrl);
        } else {
            xdsClient.storeHL7File(name, repositoryUrl);
        }
        ftpClient.deleteFile(name);
    }

    @SneakyThrows
    void copy(String name, FtpClient ftpClient) {
        @Cleanup
        final InputStream in = ftpClient.getFileStream(name);
        @Cleanup
        final FileOutputStream out = new FileOutputStream(name);
        IOUtils.copy(in, out);
    }
}
