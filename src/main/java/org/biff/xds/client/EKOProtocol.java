package org.biff.xds.client;

import ca.uhn.hl7v2.model.v26.datatype.*;
import ca.uhn.hl7v2.model.v26.message.MDM_T02;
import ca.uhn.hl7v2.model.v26.segment.OBX;
import ca.uhn.hl7v2.model.v26.segment.TXA;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.openhealthtools.mdht.uml.cda.*;
import org.openhealthtools.mdht.uml.hl7.datatypes.AD;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.*;
import org.openhealthtools.mdht.uml.hl7.vocab.*;
import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by Peter on 2016-05-19.
 */
@Value
@Slf4j
public class EKOProtocol {

    static final CDAFactory CDAF = CDAFactory.eINSTANCE;
    static final DatatypesFactory DTF = DatatypesFactory.eINSTANCE;
    static final INT ONE = DTF.createINT();
    {
        ONE.setValue(BigInteger.ONE);
    }

    private ClinicalDocument clinicalDocument;


    //
    @SneakyThrows
    public static EKOProtocol valueOf(final MDM_T02 message) {
        final ClinicalDocument cda = CDAF.createClinicalDocument();

        cda.setTypeId(CDAF.createInfrastructureRootTypeId());
        cda.getTypeId().setRoot("2.16.840.1.113883.1.3");
        cda.getTypeId().setExtension("POCD_HD000040");

        cda.getTemplateIds().addAll(Arrays.asList(
                DTF.createII("2.16.840.1.113883.10.20.6"),
                DTF.createII("2.16.840.1.113883.2.32.2.1.2.2.1")
                )
        );

        cda.setId(DTF.createII("2.25." + uid(message).toString()));
        cda.setSetId(DTF.createII("2.25." + setUid(message).toString()));

        cda.setTitle(DTF.createST("EKO-protokoll 4D Hjärtsvikt"));
        cda.setEffectiveTime(DTF.createTS(
                message.getEVN().getRecordedDateTime().getValue()
        ));

        cda.setCode(DTF.createCE("18748-4", "2.16.840.1.113883.6.1"));

        final CD translation = DTF.createCD("REPORT", "2.16.840.1.113883.2.32.2.1.2.3", null,
                message.getTXA().getDocumentType().getValue());
        final CR qualifier = DTF.createCR();
        qualifier.setValue(DTF.createCD("F", "2.16.840.1.113883.2.32.2.1.2.3.1", null, "Slutgranskat"));
        translation.getQualifiers().add(qualifier);
        cda.getCode().getTranslations().add(translation);
        cda.setConfidentialityCode(DTF.createCE("N", "2.16.840.1.113883.5.25", null, "Normal"));
        cda.setLanguageCode(DTF.createCS("sv-SE"));
        cda.setVersionNumber(ONE);

        final RecordTarget recordTarget = CDAF.createRecordTarget();
        recordTarget.setPatientRole(CDAF.createPatientRole());
        recordTarget.getPatientRole().getIds().add(
                DTF.createII(
                        "1.2.752.29.4.13",
                        message.getPID().getPatientIdentifierList(0).getIDNumber().getValue())

        );
        recordTarget.getPatientRole().setPatient(patient(message));

        cda.getAuthors().add(author(message));
        cda.getAuthors().add(device(message));
        cda.getRecordTargets().add(recordTarget);
        cda.setCustodian(custodian(message));
        cda.getInformationRecipients().add(recipient(message));
        cda.getInFulfillmentOfs().add(order(message));
        cda.setLegalAuthenticator(authenticator(message));
        cda.getParticipants().add(participant(message));
        cda.getDocumentationOfs().add(documentation(message));
        cda.setComponent(CDAF.createComponent2());
        cda.getComponent().setStructuredBody(CDAF.createStructuredBody());
        cda.getComponent().getStructuredBody().getComponents().add(basedata(message));
        cda.getComponent().getStructuredBody().getComponents().add(measuredata(message));


        return new EKOProtocol(cda);
    }

    static Component3 measuredata(final MDM_T02 message) {
        Component3 component = CDAF.createComponent3();
        component.setSection(CDAF.createSection());
        component.getSection().getTemplateIds().add(
                DTF.createII("1.3.6.1.4.1.19376.1.4.1.2.11")
        );

        component.getSection().setCode(
                DTF.createCE("59776-5", "LOINC", "LOINC", "Procedure Findings")
        );
        component.getSection().setTitle(DTF.createST("Mätvärden"));

        // left chamber
        component.getSection().getEntries().add(
                battery(message.getOBSERVATION(13).getOBX(),
                        message.getOBSERVATION(14).getOBX(),
                        message.getOBSERVATION(15).getOBX(),
                        message.getOBSERVATION(16).getOBX(),
                        message.getOBSERVATION(17).getOBX(),
                        message.getOBSERVATION(18).getOBX(),
                        message.getOBSERVATION(19).getOBX())
        );

        // right chamber
        component.getSection().getEntries().add(
                battery(message.getOBSERVATION(20).getOBX(),
                        message.getOBSERVATION(21).getOBX(),
                        message.getOBSERVATION(22).getOBX())
        );

        // left atrium
        component.getSection().getEntries().add(
                battery(message.getOBSERVATION(23).getOBX(),
                        message.getOBSERVATION(24).getOBX(),
                        message.getOBSERVATION(25).getOBX())
        );


        // diastole
        component.getSection().getEntries().add(
                battery(message.getOBSERVATION(26).getOBX(),
                        message.getOBSERVATION(27).getOBX(),
                        message.getOBSERVATION(28).getOBX(),
                        message.getOBSERVATION(29).getOBX(),
                        message.getOBSERVATION(30).getOBX(),
                        message.getOBSERVATION(31).getOBX(),
                        message.getOBSERVATION(32).getOBX(),
                        message.getOBSERVATION(33).getOBX())
        );

        // systole
        component.getSection().getEntries().add(
                battery(message.getOBSERVATION(34).getOBX(),
                        message.getOBSERVATION(35).getOBX(),
                        message.getOBSERVATION(36).getOBX())
        );


        // left
        component.getSection().getEntries().add(
                battery(message.getOBSERVATION(37).getOBX(),
                        message.getOBSERVATION(38).getOBX())
        );

        return component;
    }

    //
    @SneakyThrows
    static Component3 basedata(final MDM_T02 message) {
        Component3 component = CDAF.createComponent3();
        component.setSection(CDAF.createSection());
        component.getSection().getTemplateIds().add(
                DTF.createII("2.16.840.1.113883.10.20.22.2.4.1")
        );
        component.getSection().setCode(
                DTF.createCE("363787002", "2.16.840.1.113883.6.96", "SNOMED CT", "Observerbar företeelse")
        );
        component.getSection().setTitle(DTF.createST("Basdata"));

        IntStream.of(3, 4, 8, 9, 10, 11, 12, 13)
                .map(i -> (i - 1))
                .forEach(
                        i -> component.getSection().getEntries().add(
                                entry(message.getOBSERVATION(i).getOBX())
                        )
                );


        // tryck
        component.getSection().getEntries().add(
                battery(message.getOBSERVATION(4).getOBX(),
                        message.getOBSERVATION(5).getOBX(),
                        message.getOBSERVATION(6).getOBX())
        );

        return component;
    }




    //
    static Entry battery(final OBX parentOBX, final OBX... obxs) {
        final Entry entry = CDAF.createEntry();

        entry.setOrganizer(CDAF.createOrganizer());
        entry.getOrganizer().setClassCode(x_ActClassDocumentEntryOrganizer.BATTERY);
        entry.getOrganizer().setMoodCode(ActMood.EVN);
        entry.getOrganizer().setCode(cd(parentOBX.getObx3_ObservationIdentifier()));
        entry.getOrganizer().setStatusCode(
                DTF.createCS("completed")
        );

        for (final OBX obx: obxs) {
            final Component4 component = CDAF.createComponent4();
            component.setObservation(observation(obx));
            entry.getOrganizer().getComponents().add(component);
        }

        return entry;
    }

    //
    @SneakyThrows
    static Entry entry(final OBX obx) {
        final Entry entry = CDAF.createEntry();

        entry.setObservation(observation(obx));

        return entry;
    }

    @SneakyThrows
    static Observation observation(final OBX obx) {
        final Observation observation = CDAF.createObservation();
        observation.setCode(cd(obx.getObx3_ObservationIdentifier()));
        observation.setClassCode(ActClassObservation.OBS);
        observation.setMoodCode(x_ActMoodDocumentObservation.EVN);

        switch (obx.getObx2_ValueType().encode()) {
            case "NM":
                final NM nm = (NM) obx.getObx5_ObservationValue(0).getData();
                observation.getValues().add(
                        DTF.createPQ(
                                Double.valueOf(nm.getValue()),
                                obx.getObx6_Units().encode()
                        )
                );
                break;
            case "CWE":
                observation.getValues().add(
                        cd((CWE) obx.getObx5_ObservationValue(0).getData())
                );
                break;
            case "ST":
                observation.getValues().add(
                        DTF.createST(obx.getObx5_ObservationValue(0).getData().encode())
                );
                break;
        }

        return observation;
    }


    static CD cd(final CWE cwe) {
        final CD cd = DTF.createCD(
                cwe.getCwe1_Identifier().getValue(),
                codesystem(cwe.getCwe3_NameOfCodingSystem().getValue()),
                null,
                cwe.getCwe2_Text().getValue());

        final String altValue = cwe.getCwe5_AlternateText().getValue();
        if (StringUtils.hasText(altValue)) {
            cd.setOriginalText(DTF.createED(altValue));
        }
        return cd;
    }

    //
    @SneakyThrows
    static DocumentationOf documentation(final MDM_T02 message) {
        final DocumentationOf documentationOf = CDAF.createDocumentationOf();
        documentationOf.setServiceEvent(CDAF.createServiceEvent());

        documentationOf.getServiceEvent().setClassCode(ActClassRoot.ACT);
        documentationOf.getServiceEvent().getIds().add(
                DTF.createII(message.getOBSERVATION(0).getOBX().getObservationValue(0).encode())
        );

        // FIXME: config...
        documentationOf.getServiceEvent().getIds().add(
                DTF.createII("2.16.840.1.113883.2.32.2.1.4.2.5.2",
                        message.getCOMMON_ORDER().getOBR().getPlacerField2().getValue())
        );

        documentationOf.getServiceEvent().setEffectiveTime(
                DTF.createIVL_TS(message.getCOMMON_ORDER().getOBR().getObservationDateTime().getValue())
        );

        final CWE cwe = message.getCOMMON_ORDER().getOBR().getUniversalServiceIdentifier();

        documentationOf.getServiceEvent().setCode(
                DTF.createCE(
                        cwe.getIdentifier().getValue(),
                        codesystem(cwe.getNameOfCodingSystem().getValue()),
                        null,
                        cwe.getText().getValue()
                )
        );

        return documentationOf;
    }

    static String codesystem(final String hl7Name) {
        if (hl7Name == null) {
            return null;
        }
        switch (hl7Name) {
            case "SCT":
                return "2.16.840.1.113883.6.96";
            case "LN":
                return "2.16.840.1.113883.6.1";
            default:
                return hl7Name;
        }
    }

    //
    static Participant1 participant(final MDM_T02 message) {
        final Participant1 participant = CDAF.createParticipant1();

        participant.setTypeCode(ParticipationType.REF);
        participant.setAssociatedEntity(CDAF.createAssociatedEntity());

        participant.getAssociatedEntity().setClassCode(RoleClassAssociative.PROV);
        participant.getAssociatedEntity().getIds().add(
                hsa(
                        message.getCOMMON_ORDER().getORC().getOrc12_OrderingProvider(0).getIDNumber().getValue()
                )
        );

        participant.getAssociatedEntity().setAssociatedPerson(person(
                message.getCOMMON_ORDER().getORC().getOrc12_OrderingProvider(0)
        ));

        final Organization unit = organization(
                message.getCOMMON_ORDER().getORC().getOrc12_OrderingProvider(0).getAssigningFacility()
        );

        unit.setAsOrganizationPartOf(CDAF.createOrganizationPartOf());

        unit.getAsOrganizationPartOf().setWholeOrganization(organization(
                message.getCOMMON_ORDER().getORC().getOrc12_OrderingProvider(0).getAssigningAuthority()
        ));

        participant.getAssociatedEntity().setScopingOrganization(unit);

        return participant;
    }

    //
    static LegalAuthenticator authenticator(final MDM_T02 message) {
        final LegalAuthenticator legalAuthenticator = CDAF.createLegalAuthenticator();

        legalAuthenticator.setTime(DTF.createTS(message.getCOMMON_ORDER().getOBR().getResultsRptStatusChngDateTime().getValue()));
        legalAuthenticator.setSignatureCode(DTF.createCS("S"));
        legalAuthenticator.setAssignedEntity(CDAF.createAssignedEntity());
        legalAuthenticator.getAssignedEntity().getIds().add(
                hsa(message.getTXA().getAuthenticationPersonTimeStampSet(0).getIDNumber().getValue())
        );
        legalAuthenticator.getAssignedEntity().setAssignedPerson(
                person(message.getTXA().getAuthenticationPersonTimeStampSet(0))
        );

        return legalAuthenticator;
    }

    //
    static InformationRecipient recipient(final MDM_T02 message) {
        final InformationRecipient informationRecipient = CDAF.createInformationRecipient();

        informationRecipient.setIntendedRecipient(CDAF.createIntendedRecipient());

        final HD organization = message.getMSH().getReceivingFacility();

        informationRecipient.getIntendedRecipient()
                .setReceivedOrganization(organization(organization));

        return informationRecipient;
    }

    //
    static InFulfillmentOf order(final MDM_T02 message) {
        final InFulfillmentOf inFulfillmentOf = CDAF.createInFulfillmentOf();

        inFulfillmentOf.setOrder(CDAF.createOrder());

        inFulfillmentOf.getOrder().getTemplateIds().add(
                DTF.createII("1.3.6.1.4.1.19376.1.4.1.3.2")
        );

        final String accessionNo = message.getCOMMON_ORDER().getOBR().getPlacerField1().getValue();
        log.debug("accession: {}", accessionNo);
        inFulfillmentOf.getOrder().getIds().add(
                DTF.createII("2.16.840.1.113883.2.32.2.1.4.2.5.1", accessionNo)
        );

        final String placerId = message.getCOMMON_ORDER().getORC().getPlacerOrderNumber().getEntityIdentifier().getValue();
        log.debug("placer orderId: {}", placerId);
        inFulfillmentOf.getOrder().getIds().add(
                DTF.createII("2.16.840.1.113883.2.32.2.1.4.2.5.6", placerId)
        );

        final CWE cwe = message.getCOMMON_ORDER().getOBR().getUniversalServiceIdentifier();

        if (StringUtils.hasText(cwe.getIdentifier().getValue())) {
            inFulfillmentOf.getOrder().setCode(
                    DTF.createCE(
                            cwe.getIdentifier().getValue(),
                            codesystem(cwe.getNameOfCodingSystem().getValue()),
                            null,
                            cwe.getText().getValue()
                    )
            );
        }

        return inFulfillmentOf;
    }
    //
    static Custodian custodian(final MDM_T02 message) {
        final Custodian custodian = CDAF.createCustodian();
        custodian.setAssignedCustodian(CDAF.createAssignedCustodian());
        final CustodianOrganization custodianOrganization = CDAF.createCustodianOrganization();
        custodian.getAssignedCustodian().setRepresentedCustodianOrganization(custodianOrganization);

        custodianOrganization.getIds().add(
                hsa(
                        message
                                .getTXA()
                                .getAuthenticationPersonTimeStampSet(0)
                                .getAssigningAuthority()
                                .getNamespaceID()
                                .getValue()
                )
        );

        custodianOrganization.setName(on("Capio S:t Görans Sjukhus"));
        custodianOrganization.setAddr(ad("S:t Göransplan 1, 112 81 Stockholm"));
        custodianOrganization.setTelecom(tel("08-5870 10 00"));

        return custodian;
    }

    //
    static TEL tel(final String tel) {
        return DTF.createTEL(tel);
    }

    //
    static ON on(final String name) {
        final ON on = DTF.createON();
        on.addText(name);
        return on;
    }

    //
    static AD ad(final String addr) {
        final AD ad = DTF.createAD();
        ad.addText(addr);
        return ad;
    }

    //
    static Author device(final MDM_T02 message) {
        final Author author = CDAF.createAuthor();

        author.setFunctionCode(DTF.createCE("ASSEMBLER", "2.16.840.1.113883.5.88"));
        author.setTime(DTF.createTS(message.getMSH().getDateTimeOfMessage().getValue()));
        final AssignedAuthor assignedAuthor = CDAF.createAssignedAuthor();
        author.setAssignedAuthor(assignedAuthor);

        final HD sendingApplication = message.getMSH().getSendingApplication();
        if (StringUtils.hasText(sendingApplication.getNamespaceID().getValue())) {
            assignedAuthor.getIds().add(
                    hsa(sendingApplication.getNamespaceID().getValue())
            );
        }

        final AuthoringDevice authoringDevice = CDAF.createAuthoringDevice();
        assignedAuthor.setAssignedAuthoringDevice(authoringDevice);

        final SC softwareName = DTF.createSC();
        softwareName.addText(sendingApplication.getUniversalID().getValue());

        authoringDevice.setSoftwareName(softwareName);

        final Organization systemOwner = CDAF.createOrganization();
        assignedAuthor.setRepresentedOrganization(systemOwner);

        final HD sendingFacility = message.getMSH().getSendingFacility();

        if (StringUtils.hasText(sendingFacility.getNamespaceID().getValue())) {
            systemOwner.getIds().add(
                    hsa(sendingFacility.getNamespaceID().getValue())
            );
        }

        final ON systemOwnerName = DTF.createON();
        systemOwnerName.addText(sendingFacility.getUniversalID().getValue());
        systemOwner.getNames().add(systemOwnerName);

        systemOwner.setAsOrganizationPartOf(CDAF.createOrganizationPartOf());
        final Organization wholeOrganization = CDAF.createOrganization();
        wholeOrganization.getIds().add(
                hsa(
                        message
                                .getTXA()
                                .getAuthenticationPersonTimeStampSet(0)
                                .getAssigningAuthority()
                                .getNamespaceID()
                                .getValue()
                )
        );
        final ON wholeOrganizationName = DTF.createON();
        wholeOrganizationName.addText("Capio S:t Görans Sjukhus");
        wholeOrganization.getNames().add(wholeOrganizationName);
        systemOwner.getAsOrganizationPartOf().setWholeOrganization(wholeOrganization);

        return author;
    }

    //
    static Author author(final MDM_T02 message) {

        final Author author = CDAF.createAuthor();

        final TXA txa = message.getTXA();
        author.setFunctionCode(DTF.createCE("PRI", "2.16.840.1.113883.5.88"));
        author.setTime(DTF.createTS(message.getCOMMON_ORDER().getOBR().getResultsRptStatusChngDateTime().getValue()));

        final AssignedAuthor assignedAuthor = CDAF.createAssignedAuthor();
        author.setAssignedAuthor(assignedAuthor);

        assignedAuthor.getIds().add(
                hsa(txa.getAuthenticationPersonTimeStampSet(0).getIDNumber().getValue())
        );

        final Person assignedPerson = person(txa.getAuthenticationPersonTimeStampSet(0));

        assignedAuthor.setAssignedPerson(assignedPerson);

        assignedAuthor.setRepresentedOrganization(organization(txa.getAuthenticationPersonTimeStampSet(0).getAssigningFacility()));

        assignedAuthor.getRepresentedOrganization().setAsOrganizationPartOf(CDAF.createOrganizationPartOf());

        assignedAuthor
                .getRepresentedOrganization()
                .getAsOrganizationPartOf()
                .setWholeOrganization(
                        organization(
                                message
                                        .getTXA()
                                        .getAuthenticationPersonTimeStampSet(0)
                                        .getAssigningAuthority()
                        )
                );

        return author;
    }

    //
    static Organization organization(final HD hd) {
        final Organization org = CDAF.createOrganization();

        if (StringUtils.hasText(hd.getNamespaceID().getValue())) {
            org.getIds().add(
                    hsa(hd.getNamespaceID().getValue())
            );
        }
        final ON name = DTF.createON();
        name.addText(def(hd.getUniversalID().getValue(), "Okänd"));
        org.getNames().add(name);
        return org;
    }


    //
    static <T> T def(final T value, final T defValue) {
        return (StringUtils.isEmpty(value)) ? defValue : value;
    }

    //
    static Person person(final XCN xcn) {
        return person(xcn.getFamilyName().getSurname().getValue(),
                xcn.getGivenName().getValue());
    }

    //
    static Person person(final PPN ppn) {
        return person(
                ppn.getFamilyName().getSurname().getValue(),
                ppn.getGivenName().getValue()
        );
    }

    static Person person(final String familyName, final String givenName) {
        final Person person = CDAF.createPerson();
        final PN pn = DTF.createPN();
        person.getNames().add(pn);
        pn.getFamilies().add(
                DTF.createENXP(EntityNamePartType.FAM, def(familyName, "NA"))
        );

        pn.getGivens().add(
                DTF.createENXP(EntityNamePartType.GIV, def(givenName, "NA"))
        );

        return person;
    }


    //
    static II hsa(final String hsaId) {
        return DTF.createII("1.2.752.129.2.1.4.1", hsaId);
    }

    @SneakyThrows
    static BigInteger setUid(final MDM_T02 message) {

        final String patientId = message.getPID().getPid3_PatientIdentifierList(0).getCx1_IDNumber().getValue();
        final String requestId = message.getCOMMON_ORDER().getOBR().getPlacerField1().getValue();

        log.debug("setUid (patientId: {}, requestId: {})", patientId, requestId);

        final BigInteger id = new BigInteger(1, digest(requestId.getBytes("UTF-8")))
                .or(BigInteger.valueOf(Long.valueOf(patientId)));

        log.debug("setUid: {}", id.toString());

        return id;
    }

    //
    @SneakyThrows
    static byte[] digest(final byte[] bytes) {
        final MessageDigest md = MessageDigest.getInstance("MD5");
        return md.digest(bytes);
    }

    @SneakyThrows
    static BigInteger uid(final MDM_T02 message) {
        // FIXME: document id currently is null, use timestamo and setUid as a aworkaorund!
        final String msgTimestamp = message.getMSH().getDateTimeOfMessage().getValue();
        log.debug("uid (msgTimestamp: {}", msgTimestamp);

        final BigInteger id = setUid(message)
                .or(BigInteger.valueOf(Long.valueOf(msgTimestamp)));

        log.debug("uid: {}", id.toString());

        return id;
    }

    //
    static Patient patient(final MDM_T02 message) {
        final Patient patient = CDAF.createPatient();

        final XPN xpn = message.getPID().getPatientName(0);

        final PN pn = DTF.createPN();

        pn.getFamilies().add(
                DTF.createENXP(EntityNamePartType.FAM, xpn.getFamilyName().getSurname().getValue())
        );

        pn.getGivens().add(
                DTF.createENXP(EntityNamePartType.GIV, xpn.getGivenName().getValue())
        );

        final String middle = xpn.getSecondAndFurtherGivenNamesOrInitialsThereof().getValue();
        if (!StringUtils.isEmpty(middle)) {
            pn.getGivens().add(
                    DTF.createENXP(EntityNamePartType.GIV, middle)
            );
        }

        patient.getNames().add(pn);

        patient.setBirthTime(DTF.createTS(message.getPID().getDateTimeOfBirth().getValue()));
        String genderCode;
        switch (message.getPID().getAdministrativeSex().getValue()) {
            case "M":
                genderCode = "M";
                break;
            case "F":
                genderCode = "F";
                break;
            default:
                genderCode = "UN";
                break;
        }
        patient.setAdministrativeGenderCode(DTF.createCE(
                genderCode,
                "2.16.840.1.113883.5.1")
        );

        return patient;
    }

}
