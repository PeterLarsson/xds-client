package org.biff.xds.client;

import ca.uhn.hl7v2.model.v26.message.MDM_T02;
import ca.uhn.hl7v2.parser.PipeParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import ihe.iti.xds_b._2007.ProvideAndRegisterDocumentSetRequestType;
import lombok.Cleanup;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import oasis.names.tc.ebxml_regrep.xsd.lcm._3.SubmitObjectsRequest;
import oasis.names.tc.ebxml_regrep.xsd.rim._3.*;
import oasis.names.tc.ebxml_regrep.xsd.rs._3.RegistryResponseType;
import org.apache.commons.io.IOUtils;
import org.eclipse.emf.common.util.EList;
import org.openehealth.ipf.modules.cda.CDAR2Parser;
import org.openehealth.ipf.modules.cda.CDAR2Renderer;
import org.openehealth.ipf.modules.cda.CDAR2Validator;
import org.openhealthtools.mdht.uml.cda.AssignedAuthor;
import org.openhealthtools.mdht.uml.cda.ClinicalDocument;
import org.openhealthtools.mdht.uml.cda.Patient;
import org.openhealthtools.mdht.uml.hl7.datatypes.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.mime.MimeContainer;
import org.springframework.oxm.mime.MimeMarshaller;
import org.springframework.util.StringUtils;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapMessageFactory;
import org.springframework.ws.soap.addressing.client.ActionCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.springframework.ws.transport.http.HttpUrlConnectionMessageSender;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBElement;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


//
@Slf4j
public class XDSClient extends WebServiceGatewaySupport {

    @Autowired
    CDAR2Parser cdar2Parser;

    @Autowired
    CDAR2Validator cdar2Validator;

    @Autowired
    PipeParser pipeParser;

    @Autowired
    CDAR2Renderer cdar2Renderer;


    @Value("${practice.code}")
    String practiceCode;
    @Value("${practice.codeSystem}")
    String practiceCodeSystem;
    @Value("${practice.displayName}")
    String practiceDisplayName;

    @Value("${feature.encodeContentId}")
    boolean encodeContentId;

    final ihe.iti.xds_b._2007.ObjectFactory xdsObjectFactory = new ihe.iti.xds_b._2007.ObjectFactory();

    static final String EKO_NAME = "EKO";

    private AtomicInteger idSeq = new AtomicInteger(1);

    static ObjectMapper objectMapper = new ObjectMapper();

    public XDSClient(final SoapMessageFactory messageFactory) {
        super(messageFactory);
        setMessageSender(new HttpUrlConnectionMessageSender());
    }

    @Data
    static class AuthorBean {
        String id;
        String function;
        String[] names;
        String unit;
        String organization;
    }

    //
    @SneakyThrows
    ClinicalDocument parseCDA(final String fileName) {
        @Cleanup
        final InputStream in = new FileInputStream(fileName);
        log.debug("Unmarshal {}", fileName);
        final ClinicalDocument clinicalDocument = cdar2Parser.parse(in);
        // FIXME: hack to pass BFT validation
        if (StringUtils.isEmpty(clinicalDocument.getConfidentialityCode().getDisplayName())) {
            clinicalDocument.getConfidentialityCode().setDisplayName("Normal");
        }
        cdar2Validator.validate(clinicalDocument, null);
        log.debug("Unmarshal done");

        return clinicalDocument;
    }

    @SneakyThrows
    ClinicalDocument parseHL7(final String fileName) {
        final InputStream in = new FileInputStream(fileName);
        log.debug("Unmarshal {}", fileName);
        final String source = IOUtils.toString(in, "ISO-8859-15");
        final MDM_T02 message = (MDM_T02) pipeParser.parse(source);
        final ClinicalDocument clinicalDocument = EKOProtocol.valueOf(message).getClinicalDocument();
        cdar2Validator.validate(clinicalDocument, null);
        log.debug("Unmarshal done");

        return clinicalDocument;
    }


    @SneakyThrows
    byte[] toBytes(final String fileName) {
        @Cleanup
        final InputStream in = new FileInputStream(fileName);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(in, out);
        return out.toByteArray();
    }

    @SneakyThrows
    byte[] toBytes(final ClinicalDocument clinicalDocument) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        cdar2Renderer.render(clinicalDocument, out);
        return out.toByteArray();
    }


    @SneakyThrows
    void storeClinicalDocument(final ClinicalDocument clinicalDocument, final String fileName, final String repositoryUrl) {
        final String documentUUID = uuid();
        final ProvideAndRegisterDocumentSetRequestType provideAndRegister = new ProvideAndRegisterDocumentSetRequestType();
        provideAndRegister.setSubmitObjectsRequest(createSubmitObjectsRequest(clinicalDocument, documentUUID, fileName));

        final ProvideAndRegisterDocumentSetRequestType.Document document = new ProvideAndRegisterDocumentSetRequestType.Document();
        document.setId(documentUUID);
        document.setValue(toBytes(clinicalDocument));
        provideAndRegister.getDocument().add(document);

        log.debug("Storing document to {}", repositoryUrl);
        dump(provideAndRegister);

        Object response = getWebServiceTemplate()
                .sendAndReceive(repositoryUrl, requestMessage -> {

                    final SaajSoapMessage soapMessage = (SaajSoapMessage) requestMessage;

                    soapMessage.getSaajMessage().getSOAPPart().addMimeHeader("Content-ID", "<" + uuid() + "@grattis>");

                    try {
                        new ActionCallback("urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b").doWithMessage(requestMessage);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    ((MimeMarshaller) getMarshaller()).marshal(
                            xdsObjectFactory.createProvideAndRegisterDocumentSetRequest(provideAndRegister),
                            requestMessage.getPayloadResult(),
                            new MimeContainer() {
                                @Override
                                public boolean isXopPackage() {
                                    return soapMessage.isXopPackage();
                                }

                                @Override
                                public boolean convertToXopPackage() {
                                    return soapMessage.convertToXopPackage();
                                }

                                @Override
                                public void addAttachment(String contentId, DataHandler dataHandler) {
                                    log.info("addAttachment: {} encode: {}", contentId, encodeContentId);
                                    if (encodeContentId) {
                                        contentId = contentId.replaceAll("@", "%40");
                                    }
                                    soapMessage.addAttachment(contentId, dataHandler);
                                }

                                @Override
                                public DataHandler getAttachment(String contentId) {
                                    log.info("getAttachment {}", contentId);
                                    return soapMessage.getAttachment(contentId).getDataHandler();
                                }
                            });

                }, responseMessage -> {
                    return responseMessage;
                });

        if (response instanceof SaajSoapMessage) {
            final JAXBElement<RegistryResponseType> element = (JAXBElement<RegistryResponseType>) getUnmarshaller().unmarshal(((SaajSoapMessage) response).getPayloadSource());
            final String status = element.getValue().getStatus();
            if ("urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success".equals(status)) {
                log.info("OK");
            } else {
                log.error(status);
                log.error(objectMapper.writeValueAsString(element.getValue()));
                throw new IllegalStateException(status);
            }
        } else {
            log.error(String.valueOf(response));
            throw new IllegalStateException("Invalid and unknown response from: " + repositoryUrl);
        }
    }

    //
    public void storeCDAFile(final String cdaFileName, final String repositoryUrl) {
        storeClinicalDocument(parseCDA(cdaFileName), cdaFileName, repositoryUrl);
    }

    //
    public void storeHL7File(final String hl7FileName, final String repositoryUrl) {
        storeClinicalDocument(parseHL7(hl7FileName), hl7FileName + ".xml", repositoryUrl);
    }



    @SneakyThrows
    void dump(Object o) {
        if (log.isDebugEnabled()) {
            log.debug(objectMapper.writeValueAsString(o));
        }
    }

    void abort() {
        throw new IllegalStateException();
    }

    //
    SubmitObjectsRequest createSubmitObjectsRequest(final ClinicalDocument clinicalDocument,
                                                    final String documentUUID,
                                                    final String fileName) {
        final SubmitObjectsRequest request = new SubmitObjectsRequest();
        final RegistryObjectListType list = new RegistryObjectListType();
        request.setRegistryObjectList(list);

        final oasis.names.tc.ebxml_regrep.xsd.rim._3.ObjectFactory rimObjectFactory = new oasis.names.tc.ebxml_regrep.xsd.rim._3.ObjectFactory();
        // Extrinsic Object
        final ExtrinsicObjectType documentObject = new ExtrinsicObjectType();
        list.getIdentifiable().add(rimObjectFactory.createExtrinsicObject(documentObject));

        documentObject.setMimeType("text/xml");
        documentObject.setId(documentUUID);
        documentObject.setObjectType("urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1");

        documentObject.getSlot().add(slot("creationTime", stripTimeZone(clinicalDocument.getEffectiveTime().getValue())));
        documentObject.getSlot().add(slot("languageCode", clinicalDocument.getLanguageCode().getCode()));

        final II patientId = first(first(clinicalDocument.getRecordTargets()).getPatientRole().getIds());

        final Patient patient = first(clinicalDocument.getRecordTargets()).getPatientRole().getPatient();

        final String patientFamilyName = first(first(patient.getNames()).getFamilies()).getText();

        final String patientGivenNames = first(patient.getNames()).getGivens()
                .stream()
                .map(enxp -> enxp.getText())
                .collect(Collectors.joining(","));


        final String acessionNo = first(first(clinicalDocument.getInFulfillmentOfs()).getOrder().getIds()).getExtension();
        final String studyId = first(clinicalDocument.getDocumentationOfs()).getServiceEvent().getIds()
                .stream()
                .filter(ii -> "2.16.840.1.113883.2.32.2.1.4.2.5.2".equals(ii.getRoot()))
                .map(II::getExtension)
                .findFirst()
                .orElse(EKO_NAME);

        documentObject.getSlot().add(slot("urn:sectra:iti:xds:2015:referenceAndStudyIdList",
                String.format("%s&%s^^^^urn:sectra:iti:xds:2015:accessionAndStudyId", acessionNo, studyId)));

        documentObject.getSlot().add(slot("urn:ihe:iti:xds:2013:referenceIdList",
                String.format("%s^^^^urn:ihe:iti:xds:2013:accession", acessionNo)));

        final String sourcePatientId = String.format("%s^^^&%s&ISO",
                patientId.getExtension(), patientId.getRoot());

        documentObject.getSlot().add(slot("sourcePatientId", sourcePatientId));

        documentObject.getSlot().add(slot("sourcePatientInfo",
                "PID-3|" + sourcePatientId,
                "PID-5|" + patientFamilyName + "^" + patientGivenNames + "^^^^^^",
                "PID-8|" + patient.getAdministrativeGenderCode().getCode()));


        documentObject.getSlot().add(slot("creationTime", clinicalDocument.getEffectiveTime().getValue()));

        documentObject.getSlot().add(slot("serviceStartTime",
                first(clinicalDocument.getDocumentationOfs()).getServiceEvent().getEffectiveTime().getValue()));

        documentObject.setName(value(EKO_NAME));
        documentObject.setDescription(value("EKO-protokoll 4D Hjärtsvikt"));

        // Author
        final AuthorBean authorBean = author(clinicalDocument);
        documentObject.getClassification().add(
                classification("urn:uuid:93606bcf-9494-43ec-9b4e-a7748d1a838d",
                        null,
                        documentObject.getId(),
                        null,
                        slot("urn:uuid:93606bcf-9494-43ec-9b4e-a7748d1a838d", authorBean.getNames()),
                        slot("authorRole", authorBean.getFunction()),
                        slot("authorInstitution", authorBean.getUnit(), authorBean.getOrganization())
                )
        );


        // From serviceEvent.code
        final CE examCode = first(clinicalDocument.getDocumentationOfs()).getServiceEvent().getCode();
        documentObject.getClassification().add(
                classification("urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a",
                        documentObject.getId(),
                        first(clinicalDocument.getDocumentationOfs()).getServiceEvent().getCode())
        );

        // From getConfidentialityCode
        documentObject.getClassification().add(
                classification("urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f",
                        documentObject.getId(),
                        clinicalDocument.getConfidentialityCode())
        );


        // formatCode
        documentObject.getClassification().add(
                classification("urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d",
                        "urn:ihe:card:imaging:2011",
                        documentObject.getId(),
                        "urn:ihe:card:imaging:2011",
                        slot("codingScheme", "1.3.6.1.4.1.19376.1.2.3")
                )
        );


        // From assignedAuthor and the ASSEMBLER
        final AssignedAuthor assignedAuthoringDevice = clinicalDocument.getAuthors()
                .stream()
                .filter(a -> "ASSEMBLER".equals(a.getFunctionCode().getCode()))
                .map(a -> a.getAssignedAuthor())
                .findFirst().get();


        // FIXME: hard code to ECHO
        final II sourceDevice = first(assignedAuthoringDevice.getIds());
        /**
        final II representedOrganization = first(assignedAuthoringDevice.getRepresentedOrganization().getIds());
        final String unitName = first(assignedAuthoringDevice.getRepresentedOrganization().getNames()).getText(true);
         */
        documentObject.getClassification().add(
                classification("urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1",
                        "ECHO",
                        documentObject.getId(),
                        "Echocardiography",
                        slot("codingScheme", "2.16.840.1.113883.5.11"))
        );

        // Practice from local configuration
        documentObject.getClassification().add(
                classification("urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead",
                        practiceCode,
                        documentObject.getId(),
                        practiceDisplayName,
                        slot("codingScheme", practiceCodeSystem))
        );

        // From document code & title
        final ClassificationType typeCode = classification("urn:uuid:f0306f51-975f-434e-a61c-c59651d33983",
                clinicalDocument.getCode().getCode(),
                documentObject.getId(),
                clinicalDocument.getTitle().getText(),
                slot("codingScheme", clinicalDocument.getCode().getCodeSystem()));
        documentObject.getClassification().add(typeCode);

        documentObject.getExternalIdentifier().add(identifier(documentUUID, "XDSDocumentEntry.patientId", sourcePatientId, "urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427"));
        documentObject.getExternalIdentifier().add(identifier(documentUUID, "XDSDocumentEntry.uniqueId", clinicalDocument.getId().getRoot(), "urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab"));


        // RegistryPackage
        final RegistryPackageType registryPackage = new RegistryPackageType();
        registryPackage.setId(uuid());
        registryPackage.getSlot().add(slot("submissionTime", time()));
        // kör på klinfys!!!
        registryPackage.setName(value("Klinfys undersökning"));

        final ClassificationType setAuthor = new ClassificationType();
        setAuthor.setClassifiedObject(registryPackage.getId());
        setAuthor.setId(id());
        setAuthor.setClassificationScheme("urn:uuid:a7058bb9-b4e4-4307-ba5b-e3f0ab85e12d");
        setAuthor.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification");

        setAuthor.getSlot().add(slot("authorPerson", authorBean.getNames()));
        setAuthor.getSlot().add(slot("authorRole", authorBean.getFunction()));
        setAuthor.getSlot().add(slot("authorInstitution", authorBean.getUnit(), authorBean.getOrganization()));

        registryPackage.getClassification().add(setAuthor);

        final ClassificationType setContentTypeCode = new ClassificationType();
        setContentTypeCode.setClassifiedObject(registryPackage.getId());
        setContentTypeCode.setId(id());
        setContentTypeCode.setClassificationScheme("urn:uuid:aa543740-bdda-424e-8c96-df4873be8500");
        setContentTypeCode.setObjectType(typeCode.getObjectType());
        setContentTypeCode.setNodeRepresentation(typeCode.getNodeRepresentation());
        setContentTypeCode.getSlot().addAll(typeCode.getSlot());
        setContentTypeCode.setName(typeCode.getName());
        registryPackage.getClassification().add(setContentTypeCode);

        // Use UID for the actual document UUID
        registryPackage.getExternalIdentifier().add(identifier(registryPackage.getId(), "XDSSubmissionSet.uniqueId", uid(documentUUID), "urn:uuid:96fdda7c-d067-4183-912e-bf5ee74998a8"));
        registryPackage.getExternalIdentifier().add(identifier(registryPackage.getId(), "XDSSubmissionSet.sourceId", sourceDevice.getExtension(), "urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832"));
        registryPackage.getExternalIdentifier().add(identifier(registryPackage.getId(), "XDSSubmissionSet.patientId", sourcePatientId, "urn:uuid:6b5aea1a-874d-4603-a4bc-96a0a7b38446"));
        list.getIdentifiable().add(rimObjectFactory.createRegistryPackage(registryPackage));

        final ClassificationType submissionSet = new ClassificationType();
        submissionSet.setClassifiedObject(registryPackage.getId());
        submissionSet.setId(uuid());
        submissionSet.setClassificationNode("urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd");
        submissionSet.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification");
        list.getIdentifiable().add(rimObjectFactory.createClassification(submissionSet));

        final AssociationType1 association = new AssociationType1();
        association.setAssociationType("urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember");
        association.setSourceObject(registryPackage.getId());
        association.setTargetObject(documentObject.getId());
        association.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Association");
        association.setId(uuid());
        association.getSlot().add(slot("SubmissionSetStatus", "Original"));

        list.getIdentifiable().add(rimObjectFactory.createAssociation(association));

        return request;
    }

    //
    ClassificationType classification(final String schemeUri, final String nodeRepresentation,
                                      final String objectId, final String name, final SlotType1... slots) {
        final ClassificationType clt = new ClassificationType();
        clt.setClassifiedObject(objectId);
        clt.setId(id());
        clt.setClassificationScheme(schemeUri);
        if (!StringUtils.isEmpty(nodeRepresentation)) {
            clt.setNodeRepresentation(nodeRepresentation);
        }
        if (slots.length > 0) {
            clt.getSlot().addAll(Arrays.asList(slots));
        }
        clt.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification");
        if (!StringUtils.isEmpty(name)) {
            clt.setName(value(name));
        }
        return clt;
    }

    //
    ClassificationType classification(final String schemeUri, final String objectId, final CE code) {
        return classification(
                schemeUri,
                code.getCode(),
                objectId,
                code.getDisplayName(),
                slot("codingScheme", code.getCodeSystem())
        );
    }

    //
    private String[] givens(final EList<PN> list) {
        return
                list.stream()
                        .flatMap(pns -> pns.getGivens().stream())
                        .map(s -> s.getText())
                        .collect(Collectors.toList())
                        .stream()
                        .toArray(String[]::new);
    }


    //
    private String[] names(final EList<ON> list) {
        return
                list.stream()
                        .map(s -> s.getText(true))
                        .collect(Collectors.toList())
                        .stream()
                        .toArray(String[]::new);
    }

    //
    String uuid() {
        return "urn:uuid:" + UUID.randomUUID().toString();
    }

    //
    String id() {
        return "id_" + idSeq.incrementAndGet();
    }

    //
    ExternalIdentifierType identifier(final String documentId, final String name, final String value, final String scheme) {
        final ExternalIdentifierType identifier = new ExternalIdentifierType();
        identifier.setName(value(name));
        identifier.setValue(value);
        identifier.setIdentificationScheme(scheme);
        identifier.setId(id());
        identifier.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier");
        identifier.setRegistryObject(documentId);
        return identifier;
    }

    //
    SlotType1 slot(String name, String... values) {
        final SlotType1 slotType = new SlotType1();
        slotType.setName(name);
        final ValueListType valueListType = new ValueListType();
        for (final String value : values) {
            valueListType.getValue().add(value);
        }
        slotType.setValueList(valueListType);
        return slotType;
    }


    //
    InternationalStringType value(final String value) {
        final InternationalStringType internationalStringType = new InternationalStringType();
        final LocalizedStringType localizedStringType = new LocalizedStringType();
        localizedStringType.setValue(value);
        localizedStringType.setLang("sv-SE");
        localizedStringType.setCharset("UTF-8");
        internationalStringType.getLocalizedString().add(localizedStringType);
        return internationalStringType;
    }

    //
    AuthorBean author(final ClinicalDocument clinicalDocument) {

        final AuthorBean authorBean = new AuthorBean();

        clinicalDocument.getAuthors()
                .stream()
                .filter(a -> "PRI".equals(a.getFunctionCode().getCode()))
                .peek(p -> {
                    if (authorBean.getFunction() == null) {
                        authorBean.setFunction(p.getFunctionCode().getCode());
                    }
                })
                .map(a -> a.getAssignedAuthor())
                .filter(a -> a.getAssignedPerson() != null)
                .peek(p -> {
                    authorBean.setId(firstId(p.getIds()));
                    authorBean.setNames(givens(p.getAssignedPerson().getNames()));

                    final String[] unitNames = names(p.getRepresentedOrganization().getNames());
                    final String unitName =
                            firstId(p.getRepresentedOrganization().getIds())
                                    + ((unitNames.length > 0) ? " (" + unitNames[0] + ")" : "");
                    authorBean.setUnit(unitName);

                    final String[] orgNames = names(p.getRepresentedOrganization().getAsOrganizationPartOf().getWholeOrganization().getNames());
                    final String orgName =
                            firstId(p.getRepresentedOrganization().getAsOrganizationPartOf().getWholeOrganization().getIds())
                                    + ((orgNames.length > 0) ? " (" + orgNames[0] + ")" : "");
                    authorBean.setOrganization(orgName);
                })
                .count();

        return authorBean;
    }

    //
    String date() {
        return LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE);
    }

    //
    String time() {
        return date();
    }

    //
    String stripTimeZone(final String date) {
        final int index = date == null ? -1 : date.lastIndexOf('+');
        if (index != -1) {
            return date.substring(0, index);
        }
        return date;
    }

    //
    String firstId(final List<II> list) {
        final II ii = first(list);
        return (ii == null) ? null : ii.getExtension();
    }

    //
    <T> T first(final List<T> list) {
        return (list == null || list.isEmpty()) ? null : list.get(0);
    }


    //
    String uid(String uuidString) {

        if (uuidString.startsWith("urn:uuid:")) {
            uuidString = uuidString.substring("urn:uuid:".length());
        }

        final UUID uuid = UUID.fromString(uuidString);

        final BigInteger value = unsigned(uuid.getMostSignificantBits())
                .shiftLeft(64)
                .or(unsigned(uuid.getLeastSignificantBits()));

        return "2.25." + value.toString();
    }

    //
    BigInteger unsigned(final long n) {
        return new BigInteger(1, bytes(n));
    }

    //
    byte[] bytes(final long n) {
        return ByteBuffer.allocate(Long.BYTES).putLong(n).array();
    }
}
