
package org.biff.xds.client;

import ca.uhn.hl7v2.parser.PipeParser;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openehealth.ipf.modules.cda.CDAR2Parser;
import org.openehealth.ipf.modules.cda.CDAR2Renderer;
import org.openehealth.ipf.modules.cda.CDAR2Utils;
import org.openehealth.ipf.modules.cda.CDAR2Validator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import javax.xml.bind.Marshaller;
import java.util.Collections;


@Configuration
@Slf4j
public class WebServiceConfig {
	@Bean
	@SneakyThrows
	public Jaxb2Marshaller marshaller() {
		final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan(
				"org.hl7.v3",
				"ihe.iti.xds_b._2007",
				"oasis.names.tc.ebxml_regrep.xsd.lcm._3",
				"oasis.names.tc.ebxml_regrep.xsd.query._3",
				"oasis.names.tc.ebxml_regrep.xsd.rim._3",
				"oasis.names.tc.ebxml_regrep.xsd.rs._3"
		);
		marshaller.setMtomEnabled(true);
		marshaller.setMarshallerProperties(Collections.singletonMap(Marshaller.JAXB_FORMATTED_OUTPUT, true));
		return marshaller;
	}

	@Bean
	public CDAR2Parser cdar2Parser() {
		return new CDAR2Parser();
	}

	@Bean
	public CDAR2Renderer cdar2Renderer() {
		return new CDAR2Renderer();
	}

	@Bean
	public PipeParser pipeParser() {
		return new PipeParser();
	}

	@Bean
	public CDAR2Validator cdar2Validator() {
		CDAR2Utils.initCCD();
		return new CDAR2Validator();
	}

	@Bean
	public SaajSoapMessageFactory saajSoapMessageFactory() {
		final SaajSoapMessageFactory factory = new SaajSoapMessageFactory();
		factory.setSoapVersion(SoapVersion.SOAP_12);
		return factory;
	}

	@Bean
	public XDSClient xdsClient(final Jaxb2Marshaller marshaller) {
		final XDSClient client = new XDSClient(saajSoapMessageFactory());
		client.setDefaultUri("http://192.168.99.100:8080/xdstools2/sim/1__but/rep/prb");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

}
