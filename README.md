# README #

Litet verktyg för att dels konvertera EKO HL7v2 protokoll till motsvarande CDA och dels ladda upp CDA via standard XDS gränssnitt till ett VNA.

### Förutsättningar ###

 * Java 8
 * Git

### Bygga ###

~~~
$ ./gradlew build
~~~

### Köra ###

~~~
$ java -jar build/libs/xds-client-1.0-SNAPSHOT.jar [ <hl7-eko-protokoll>.hl7 | <cda>.xml | <user>:<pass>@<ftp-server> ] <xds-repository-url>
~~~